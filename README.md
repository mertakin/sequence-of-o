# Sequence of O

![](/cover.png)

## Introduction

Sequence of O is a Modelling and Simulation System in Cellular Biology, which is applied a visual art approach.

Cellular biology is the basis of our project. The correct examination of this basis and the Elements that should be a part of visual simulation create the structure of our model.

## Motivation

The purpose of the project is to make a modelling cellular biological phenomena and make this model an interactive visual art element by applying it
[Three-act structure](https://en.wikipedia.org/wiki/Three-act_structure)

Cellular biological phenomena will be examined and the elements which are suitable for visual simulation will form the structure of the model. 
This modeling, which will give an idea about the study area, will be used and interpretable by associating with other fields

## Structure

Cellular biology elements; Structure and Function, Cell Signalling, Cell Cycle in setup section as Act I; Mutations, Virus and Bacteria in confrontation section as Act II;
Cell Death and Reneval in result section as Act III can be used in The 3-Act Structure.

In our project has four of these elements: Structure and Function, Cell Cycle, Mutations and Cell Death and Reneval.

![](structure.png)

## Tools

- Node.js
- p5-manager
- p5.js library

## Demonstration

[Live demo is available on github pages](https://mertakin.github.io/sequence-of-o/)

![](soo.png)

